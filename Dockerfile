FROM debian:latest
LABEL MAINTAINER="tanguy@baudrin.fr"
LABEL source.url="https://gitlab.com/Tangeek59/dockerized-domoticz.git"
# Inspired by domoticz install documentation available here : https://www.domoticz.com/wiki/Linux

# Updating distribution packages
RUN apt-get update -q
RUN apt-get upgrade -yq

# Installing domoticz dependencies
RUN apt-get install build-essential cmake git libboost-dev libboost-thread-dev libboost-system-dev libsqlite3-dev curl libcurl4-openssl-dev libssl-dev libusb-dev zlib1g-dev python3-dev libudev-dev -yq
RUN apt-get clean -yq

# Download & install OpenZWave
RUN curl http://mirror.my-ho.st/Downloads/OpenZWave/Debian_8.0/amd64/libopenzwave1.3_1.4.164_amd64.deb -o libopenzwave1.3_1.4.164_amd64.deb
RUN curl http://mirror.my-ho.st/Downloads/OpenZWave/Debian_8.0/amd64/openzwave_1.4.164_amd64.deb -o openzwave_1.4.164_amd64.deb
RUN dpkg -i libopenzwave1.3_1.4.164_amd64.deb
RUN dpkg -i openzwave_1.4.164_amd64.deb

# Creating domoticz user and setting domoticz folder
RUN useradd -s /bin/bash domoticz
RUN mkdir /var/domoticz
RUN chown domoticz:domoticz /var/domoticz
USER domoticz

# Cloning the github repository and making project
RUN git clone -q https://github.com/domoticz/domoticz.git /var/domoticz
WORKDIR /var/domoticz
RUN git checkout origin/master
RUN cmake -Wno-dev -USE_STATIC_OPENZWAVE -DCMAKE_BUILD_TYPE=Release .
RUN make -s

# Prepare running domoticz
EXPOSE 8080
ENTRYPOINT /var/domoticz/domoticz